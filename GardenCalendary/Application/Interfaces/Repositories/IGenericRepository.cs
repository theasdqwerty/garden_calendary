﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Repositories
{
    public interface IGenericRepository<T, M> where T : class where M : class
    {
        Task<M> GetByIdAsync(int id);
        Task<List<M>> GetAllAsync();
        Task AddAsync(M model);
        Task UpdateAsync(int id, M model);
        Task DeleteAsync(int id);
    }
}
