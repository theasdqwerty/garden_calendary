﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services
{
    public interface ISteadService
    {
        Task<List<SteadModel>> GetAllSteadsAsync();
        Task<SteadModel> GetSteadAsync(int id);
        Task AddSteadAsync(SteadModel stead);
        Task ChangeSteadAsync(int id, SteadModel stead);
        Task RemoveSteadAsync(int id);
    }
}
