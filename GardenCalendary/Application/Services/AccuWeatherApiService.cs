﻿using Application.Interfaces.Services;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class AccuWeatherApiService : IAccuWeatherApiService
    {
        public static string apiKey = "bwT0t11YQ9s2mGLbJ2jiMC1B0zhoE7LL";

        /// <summary>
        /// Метод для получения id и наименования города
        /// </summary>
        /// <param name="geoPosition"></param>
        /// <param name="langauage"></param>
        /// <param name="details"></param>
        /// <param name="toplevel"></param>
        public void GetCityFromServices(GeoPositionModel geoPosition, string langauage = "ru", bool details = true, bool toplevel = false)
        {
            string latLon = String.Concat(Math.Floor(geoPosition.Latitude), "%2C",  (int)((geoPosition.Latitude - Math.Floor(geoPosition.Latitude)) * 100000), "%2C",
                Math.Floor(geoPosition.Longitude), "%2C", (int)((geoPosition.Longitude - Math.Floor(geoPosition.Longitude)) * 100000));
            //43.2380, 76.8829
            try
            {
                string jsonOnWeb = $"http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey={apiKey}&q={latLon}&language={langauage}&details={details}&toplevel={toplevel}";
                WebClient webClient = new();
                string prepareString = webClient.DownloadString(jsonOnWeb);
                //TO DO: переименовать метод на getcitybygeoposition, возвращать объект
            }
            catch
            {
               
            }
        }

        /// <summary>
        /// Метод реализует получение списка городов
        /// </summary>
        /// <param name="сityName"></param>
        /// <param name="langauage"></param>
        /// <param name="details"></param>
        public void GetCityFromServices(string сityName, string langauage = "ru", bool details = true)
        {
            try
            {
                string jsonOnWeb = $"http://dataservice.accuweather.com/locations/v1/cities/search?apikey={apiKey}&q={сityName}&language={langauage}&details={details}";
                WebClient webClient = new();
                string prepareString = webClient.DownloadString(jsonOnWeb);
            }
            catch
            {

            }
        }

        /// <summary>
        /// Метод реализует получение погоды на 1/5/10/15 дней
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="dayQuantity"></param>
        /// <param name="language"></param>
        /// <param name="metric"></param>
        public void GetWeatherFromServices(int cityId, int dayQuantity, string language = "ru", bool metric = true)
        {
            string days = String.Concat(dayQuantity, "day/");
            try
            {
                string jsonOnWeb = $"http://dataservice.accuweather.com/forecasts/v1/daily/{days}&{cityId}&apikey={apiKey}&language={language}&metric={metric}";
                WebClient webClient = new();
                string prepareString = webClient.DownloadString(jsonOnWeb);
            }
            catch
            {

            }
        }

    }
}
