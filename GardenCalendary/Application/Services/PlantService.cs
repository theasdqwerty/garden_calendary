using Application.Services.Base;
using Domain.Entities;
using Domain.Interfaces;

namespace Application.Services;

public class PlantService : BaseService<Plant>
{
    public PlantService(IRepository<Plant> repository) : base(repository)
    {
    }
}
