﻿using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SteadService : ISteadService
    {
        private readonly ISteadRepository _steadRepository;
        public SteadService(ISteadRepository steadRepository) 
        {
            _steadRepository = steadRepository;
        }

        public async Task<List<SteadModel>> GetAllSteadsAsync()
        {
            return await _steadRepository.GetAllSteadsAsync();
        }

        public async Task<SteadModel> GetSteadAsync(int id)
        {
            return await _steadRepository.GetSteadAsync(id);
        }

        public async Task AddSteadAsync(SteadModel stead)
        {
            await _steadRepository.AddSteadAsync(stead);
        }

        public async Task ChangeSteadAsync(int id, SteadModel stead)
        {
            await _steadRepository.ChangeSteadAsync(id, stead);
        }

        public async Task RemoveSteadAsync(int id)
        {
            await _steadRepository.RemoveSteadAsync(id);
        }
    }
}
