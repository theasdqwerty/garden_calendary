using Domain;
using Domain.Interfaces;

namespace Application.Services.Base;

/// <summary>
/// Basic service with CRUD operation.
/// </summary>
/// <typeparam name="T">Root model</typeparam>
public class BaseService<T> : IService<T> where T : EntityBase
{
    private readonly IRepository<T> _repository;

    protected BaseService(IRepository<T> repository)
        => _repository = repository;
    
    public async Task Insert(T entity) 
        => await _repository.Insert(entity);
    
    public async Task Update(T entity)
        => await _repository.Update(entity);

    public async Task Update(int id, T entity) 
        => await _repository.Update(id, entity);

    public async Task Delete(T entity)
        => await _repository.Delete(entity);

    public async Task Delete(int id)
        => await _repository.Delete(id);

    public async Task<T?> GetById(int id)
        => await _repository.GetById(id);

    public async Task<IList<T>> GetAll(CancellationToken token = default)
        => await _repository.List(token);
}