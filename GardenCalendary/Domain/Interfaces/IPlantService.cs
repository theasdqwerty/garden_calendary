using Domain.Entities;

namespace Domain.Interfaces;

public interface IPlantService : IService<Plant>
{
}