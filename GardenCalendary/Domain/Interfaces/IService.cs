namespace Domain.Interfaces;

// ReSharper disable once InconsistentNaming
/// <summary>
/// Basic service interface with CRUD operations.
/// </summary>
/// <typeparam name="T">Root model</typeparam>
public interface IService<T> where T: EntityBase
{
    Task Insert(T entity);
    
    Task Update(T entity);

    Task Update(int id, T entity);
    
    Task Delete(T entity);

    Task Delete(int id);
    
    Task<T?> GetById(int id);

    Task<IList<T>> GetAll(CancellationToken token = default);
}