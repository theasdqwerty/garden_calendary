namespace Domain.Interfaces;

/// <summary>
/// Basic repositoty interfance with CRUD operations.
/// </summary>
/// <typeparam name="T">Root model</typeparam>
public interface IRepository<T> where T : EntityBase
{
    Task Insert(T entity);
    
    Task Update(T entity);

    Task Update(int id, T entity);
    
    Task Delete(T entity);

    Task Delete(int id);
    
    Task<T?> GetById(int id);

    Task<IList<T>> List(CancellationToken token = default);
}
