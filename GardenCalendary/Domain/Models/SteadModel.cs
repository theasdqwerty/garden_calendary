﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class SteadModel
    {
        public int Id { get; set; }

        public int? RegionCodeId { get; set; }

        public int? ObjectId { get; set; }

        public int? UserId { get; set; }

        public int? PlantId { get; set; }

        public DateTime? StartDate { get; set; }
    }
}
