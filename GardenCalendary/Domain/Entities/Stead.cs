﻿namespace Domain.Entities;

public class Stead : EntityBase
{
    public int? RegionCodeId { get; set; }

    public int? ObjectId { get; set; }

    public int? UserId { get; set; }

    public int? PlantId { get; set; }

    public DateTime? StartDate { get; set; }

    public virtual RReestrObject? Object { get; set; }

    public virtual Plant? Plant { get; set; }

    public virtual Region? RegionCode { get; set; }

    public virtual User? User { get; set; }
}
