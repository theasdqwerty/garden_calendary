﻿namespace Domain.Entities;

public class WeatherCalendar : EntityBase
{
    public int? RegionCodeId { get; set; }

    public int? ObjectId { get; set; }

    public DateTime? DateTame { get; set; }

    public int? PrecipitationId { get; set; }

    public int? TemperaturaMax { get; set; }

    public int? TemperaturaMin { get; set; }

    public virtual RReestrObject? Object { get; set; }

    public virtual RPrecipitation? Precipitation { get; set; }

    public virtual Region? RegionCode { get; set; }
}
