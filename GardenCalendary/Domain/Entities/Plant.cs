﻿namespace Domain.Entities;

public class Plant : EntityBase
{
    public string? PlantName { get; set; }

    public int? TempMin { get; set; }

    public int? TempMax { get; set; }

    public int? SoilMoistureMin { get; set; }

    public int? SoilMoistureMax { get; set; }

    public ICollection<Stead> Steads { get; set; } = new List<Stead>();
}
