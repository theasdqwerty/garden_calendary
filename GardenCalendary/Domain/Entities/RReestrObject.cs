﻿namespace Domain.Entities;

public class RReestrObject : EntityBase
{
    public int? RegionCodeId { get; set; }

    public string? ObjectName { get; set; }

    public int? AccuweatherId { get; set; }

    public virtual Region? RegionCode { get; set; }

    public virtual ICollection<Stead> Steads { get; set; } = new List<Stead>();

    public virtual ICollection<WeatherCalendar> WeatherCalendars { get; set; } = new List<WeatherCalendar>();
}
