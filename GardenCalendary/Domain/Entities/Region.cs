﻿namespace Domain.Entities;

public class Region : EntityBase
{
    public int RegionCodeId { get; set; }

    public string? RegionName { get; set; }

    public virtual ICollection<RReestrObject> RReestrObjects { get; set; } = new List<RReestrObject>();

    public virtual ICollection<Stead> Steads { get; set; } = new List<Stead>();

    public virtual ICollection<User> Users { get; set; } = new List<User>();

    public virtual ICollection<WeatherCalendar> WeatherCalendars { get; set; } = new List<WeatherCalendar>();
}
