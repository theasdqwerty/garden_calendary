﻿namespace Domain.Entities;

public class RPrecipitation : EntityBase
{
    public string? Precipitation { get; set; }

    public virtual ICollection<WeatherCalendar> WeatherCalendars { get; set; } = new List<WeatherCalendar>();
}
