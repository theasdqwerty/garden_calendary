namespace Domain;

/// <summary>
/// Базовая сущность.
/// </summary>
public abstract class EntityBase 
{
    /// <summary>
    /// PK
    /// </summary>
    public int Id { get; set; }
}