using Microsoft.AspNetCore.Mvc;
using GardeningTipsService.Models;

namespace GardeningTipsService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RecommendationController : ControllerBase
    {
        private readonly ApplicationContext _context;
        public RecommendationController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult GetAllRecommendations()
        {
            Recommendation recommendation = new Recommendation(_context);
            return Ok(recommendation.GetAllRecommendations()); 
        }
    }
}