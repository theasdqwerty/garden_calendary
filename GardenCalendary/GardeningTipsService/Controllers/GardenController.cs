﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GardeningTipsService.Models;


namespace GardeningTipsService.Controllers
{
    public class GardenController : Controller
    {
        private readonly ApplicationContext _context;
        public GardenController(ApplicationContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult> GetAllKitchenGargens()
        {
            return Ok(await _context.KitchenGargen.ToListAsync());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return Ok(new SelectList(_context.InfoGardering, "Id", "VegetableName"));
        }
        [HttpPost("kitchenGargen")]
        public async Task<ActionResult> Create(KitchenGargen kitchenGargen)
        {
            kitchenGargen.VegetableName = _context.InfoGardering.FirstOrDefault(item => item.Id == kitchenGargen.InfoGarderingId).VegetableName;
            _context.KitchenGargen.Add(kitchenGargen);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}