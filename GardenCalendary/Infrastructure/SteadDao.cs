﻿using Domain.Entities;

namespace Infrastructure
{
    public class SteadDao : GardenDaoBase
    {
        public SteadDao(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public Stead FindById(long steadId) => GardenCalendaryContext.Stead.FirstOrDefault(it => it.Id == steadId);

        public void Add(Stead stead)
        {
            GardenCalendaryContext.Stead.Add(stead);
            SaveChanges();
        }
    }
}
