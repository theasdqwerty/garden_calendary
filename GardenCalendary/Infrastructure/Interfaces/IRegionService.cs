﻿using Domain.Entities;

namespace Infrastructure.Interfaces
{
    public interface IRegionService
    {
        void Add(Region region);
        Region FindById(long regionId);
    }
}