﻿using Domain.Entities;

namespace Infrastructure.Interfaces
{
    public interface IUserService
    {
        public void Add(User user);
        public User FindById(long userId);
    }
}