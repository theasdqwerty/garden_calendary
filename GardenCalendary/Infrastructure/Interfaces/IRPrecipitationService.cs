﻿using Domain.Entities;

namespace Infrastructure.Interfaces
{
    public interface IRPrecipitationService
    {
        void Add(RPrecipitation rPrecipitation);
        RPrecipitation FindById(long rPrecipitationId);
    }
}