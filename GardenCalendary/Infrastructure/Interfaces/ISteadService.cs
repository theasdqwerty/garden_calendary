﻿using Domain.Entities;

namespace Infrastructure.Interfaces
{
    public interface ISteadService
    {
        void Add(Stead stead);
        Stead FindById(long steadId);
    }
}