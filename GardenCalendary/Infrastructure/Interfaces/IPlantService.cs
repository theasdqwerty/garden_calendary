﻿using Domain.Entities;

namespace Infrastructure.Interfaces
{
    public interface IPlantService
    {
        void Add(Plant plant);
        Plant FindById(long plantId);
    }
}