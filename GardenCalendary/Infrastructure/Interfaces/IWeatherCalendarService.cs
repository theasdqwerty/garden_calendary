﻿using Domain.Entities;

namespace Infrastructure.Interfaces
{
    public interface IWeatherCalendarService
    {
        void Add(WeatherCalendar weatherCalendar);
        WeatherCalendar FindById(long weatherCalendarId);
    }
}