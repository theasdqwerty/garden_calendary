﻿using Domain.Entities;

namespace Infrastructure.Interfaces
{
    public interface IRReestrObjectService
    {
        void Add(RReestrObject rReestrObject);
        RReestrObject FindById(long rReestrObjectId);
    }
}