﻿using Domain.Entities;

namespace Infrastructure
{
    public class UserDao : GardenDaoBase
    {
        public UserDao(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public User FindById(long userId) => GardenCalendaryContext.User.FirstOrDefault(it => it.Id == userId);

        public void Add(User user)
        {
            GardenCalendaryContext.User.Add(user);
            SaveChanges();
        }
    }
}
