﻿using System;
using System.Collections.Generic;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public partial class GardenCalendaryContext : DbContext
{
    public GardenCalendaryContext()
    {
    }

    public GardenCalendaryContext(DbContextOptions<GardenCalendaryContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Plant> Plant { get; set; }

    public virtual DbSet<RPrecipitation> RPrecipitation { get; set; }

    public virtual DbSet<RReestrObject> RReestrObject { get; set; }

    public virtual DbSet<Region> Region { get; set; }

    public virtual DbSet<Stead> Stead { get; set; }

    public virtual DbSet<User> User { get; set; }

    public virtual DbSet<WeatherCalendar> WeatherCalendar { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseNpgsql("Host=192.168.1.104;Port=5432;Database=garden_calendary;Username=garden_calendary;Password=summer");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Plant>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("plant_pkey");

            entity.ToTable("plant");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.PlantName)
                .HasColumnType("character varying")
                .HasColumnName("plant_name");
            entity.Property(e => e.SoilMoistureMax).HasColumnName("soil_moisture_max");
            entity.Property(e => e.SoilMoistureMin).HasColumnName("soil_moisture_min");
            entity.Property(e => e.TempMax).HasColumnName("temp_max");
            entity.Property(e => e.TempMin).HasColumnName("temp_min");
        });

        modelBuilder.Entity<RPrecipitation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("precipitation_pkey");

            entity.ToTable("r$precipitation");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("id");
            entity.Property(e => e.Precipitation)
                .HasColumnType("character varying")
                .HasColumnName("precipitation");
        });

        modelBuilder.Entity<RReestrObject>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("r$reestr_object_pkey");

            entity.ToTable("r$reestr_object");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccuweatherId).HasColumnName("accuweather_id");
            entity.Property(e => e.ObjectName)
                .HasColumnType("character varying")
                .HasColumnName("object_name");
            entity.Property(e => e.RegionCodeId).HasColumnName("region_code_id");  
        });

        modelBuilder.Entity<Region>(entity =>
        {
            entity.HasKey(e => e.RegionCodeId).HasName("region_pkey");
            entity.ToTable("region");
            entity.Property(e => e.RegionCodeId)
                .ValueGeneratedNever()
                .HasColumnName("region_code_id");
            entity.Property(e => e.RegionName)
                .HasColumnType("character varying")
                .HasColumnName("region_name");
        });

        modelBuilder.Entity<Region>(entity =>
        {
            entity.HasKey(e => e.RegionCodeId).HasName("region_pkey");

            entity.ToTable("region");

            entity.Property(e => e.RegionCodeId)
                .ValueGeneratedNever()
                .HasColumnName("region_code_id");
            entity.Property(e => e.RegionName)
                .HasColumnType("character varying")
                .HasColumnName("region_name");
        });       

        modelBuilder.Entity<Stead>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("stead_pkey");

            entity.ToTable("stead");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ObjectId).HasColumnName("object_id");
            entity.Property(e => e.PlantId).HasColumnName("plant_id");
            entity.Property(e => e.RegionCodeId).HasColumnName("region_code_id");
            entity.Property(e => e.StartDate).HasColumnName("start_date");
            entity.Property(e => e.UserId).HasColumnName("user_id");

            entity.HasOne(d => d.Object).WithMany(p => p.Steads)
                .HasForeignKey(d => d.ObjectId)
                .HasConstraintName("stead_object_id_fkey");

            entity.HasOne(d => d.Plant).WithMany(p => p.Steads)
                .HasForeignKey(d => d.PlantId)
                .HasConstraintName("stead_plant_id_fkey");

            entity.HasOne(d => d.RegionCode).WithMany(p => p.Steads)
                .HasForeignKey(d => d.RegionCodeId)
                .HasConstraintName("stead_region_code_fkey");                   

            entity.HasOne(d => d.User).WithMany(p => p.Steads)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("stead_user_id_fkey");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("users_pkey");

            entity.ToTable("users");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Email)
                .HasColumnType("character varying")
                .HasColumnName("email");
            entity.Property(e => e.FirstName)
                .HasColumnType("character varying")
                .HasColumnName("first_name");
            entity.Property(e => e.LastName)
                .HasColumnType("character varying")
                .HasColumnName("last_name");
            entity.Property(e => e.PhoneNumber)
                .HasColumnType("character varying")
                .HasColumnName("phone_number");
            entity.Property(e => e.RegionCodeId).HasColumnName("region_code_id");
            entity.Property(e => e.SecondName)
                .HasColumnType("character varying")
                .HasColumnName("second_name");

            entity.HasOne(d => d.RegionCode).WithMany(p => p.Users)
                .HasForeignKey(d => d.RegionCodeId)
                .HasConstraintName("users_region_code_fkey");                   
        });

        modelBuilder.Entity<WeatherCalendar>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("weather_calendar_pkey");

            entity.ToTable("weather_calendar");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.DateTame).HasColumnName("date_tame");
            entity.Property(e => e.ObjectId).HasColumnName("object_id");
            entity.Property(e => e.PrecipitationId).HasColumnName("precipitation_id");
            entity.Property(e => e.RegionCodeId).HasColumnName("region_code_id");
            entity.Property(e => e.TemperaturaMax).HasColumnName("temperatura_max");
            entity.Property(e => e.TemperaturaMin).HasColumnName("temperatura_min");

            entity.HasOne(d => d.Object).WithMany(p => p.WeatherCalendars)
                .HasForeignKey(d => d.ObjectId)
                .HasConstraintName("weather_calendar_object_id_fkey");

            entity.HasOne(d => d.Precipitation).WithMany(p => p.WeatherCalendars)
                .HasForeignKey(d => d.PrecipitationId)
                .HasConstraintName("weather_calendar_precipitation_id_fkey");

            entity.HasOne(d => d.RegionCode).WithMany(p => p.WeatherCalendars)
                .HasForeignKey(d => d.RegionCodeId)
                .HasConstraintName("weather_calendar_region_code_fkey");                 
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
