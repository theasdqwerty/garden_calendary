﻿using Application;
using Domain.Entities;
using Infrastructure.Interfaces;

namespace Infrastructure
{
    public class UserService : GardenServiceBase, IUserService
    {
        public UserService(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public User FindById(long userId)
        {
            return GardenDaoRegistry.UserDao.FindById(userId);
        }

        public void Add(User user)
        {
            GardenDaoRegistry.UserDao.Add(user);
        }
    }
}
