﻿using Domain.Entities;

namespace Infrastructure
{
    public class RPrecipitationDao : GardenDaoBase
    {
        public RPrecipitationDao(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public RPrecipitation FindById(long pPrecipitationId) => GardenCalendaryContext.RPrecipitation.FirstOrDefault(it => it.Id == pPrecipitationId);

        public void Add(RPrecipitation pPrecipitation)
        {
            GardenCalendaryContext.RPrecipitation.Add(pPrecipitation);
            SaveChanges();
        }
    }
}
