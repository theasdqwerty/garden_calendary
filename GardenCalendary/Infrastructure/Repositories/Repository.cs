using Domain;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories;

public class Repository<T> : IRepository<T> where T : EntityBase
{
    private readonly DbContext _dbContext;

    public Repository(DbContext dbContext) 
        => _dbContext = dbContext;

    public async Task Insert(T entity)
    {
        ArgumentNullException.ThrowIfNull(entity);

        _dbContext.Set<T>().Add(entity);
        await _dbContext.SaveChangesAsync();
    }

    public async Task Update(T entity)
    {
        ArgumentNullException.ThrowIfNull(entity);

        var exist = await _dbContext.Set<T>().FirstOrDefaultAsync(o => o.Id == entity.Id);
        if (exist is not null)
        {
            _dbContext.Set<T>().Entry(exist).CurrentValues.SetValues(entity);
            await _dbContext.SaveChangesAsync();
        }
    }

    public async Task Update(int id, T entity)
    {
        var exist = await _dbContext.Set<T>().FirstOrDefaultAsync(o => o.Id == id);
        if (exist is not null)
            await Update(exist);
    }

    public async Task Delete(T entity)
    {
        ArgumentNullException.ThrowIfNull(entity);

        _dbContext.Set<T>().Remove(entity);
        await _dbContext.SaveChangesAsync();
    }

    public async Task Delete(int id)
    {
        var exist = await _dbContext.Set<T>().FirstOrDefaultAsync(o => o.Id == id);
        if (exist is not null)
            await Delete(exist);
    }

    public async Task<T?> GetById(int id)
        => await _dbContext.Set<T>().FirstOrDefaultAsync(o => o.Id == id);

    public async Task<IList<T>> List(CancellationToken token = default) 
        => await _dbContext.Set<T>().ToListAsync(token);
}