﻿using Application.Interfaces.Repositories;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class GenericRepository<T, M> : IGenericRepository<T, M> where T : class where M : class
    {
        private readonly GardenCalendaryContext _context;
        private readonly Mapper _mapper;

        public GenericRepository(GardenCalendaryContext context)
        {
            _context = context;
            _mapper = AutomapperConfig.InitializeAutomapper();
        }

        public async Task AddAsync(M model)
        {
            T entity = _mapper.Map<T>(model);
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(int id, M model)
        {
            T entity = _mapper.Map<T>(model);
            T exist = await _context.Set<T>().FindAsync(id);
            if (exist != null)
            {
                _context.Entry(exist).CurrentValues.SetValues(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            T entity = await _context.Set<T>().FindAsync(id);
            if (entity != null)
            {
                _context.Set<T>().Remove(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<List<M>> GetAllAsync()
        {
            List<T> values = await _context
                .Set<T>()
                .ToListAsync();
            var models = new List<M>();
            foreach (var entity in values)
            {
                models.Add(_mapper.Map<M>(entity));
            }
            return models;
        }

        public async Task<M> GetByIdAsync(int id)
        {
            return _mapper.Map<M>(await _context.Set<T>().FindAsync(id));
        }
    }
}
