﻿using Application.Interfaces.Repositories;
using Domain.Entities;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class SteadRepository : ISteadRepository
    {
        private readonly IGenericRepository<Stead, SteadModel> _repository;
        public SteadRepository(IGenericRepository<Stead, SteadModel> repository)
        {
            _repository = repository;
        }

        public async Task<List<SteadModel>> GetAllSteadsAsync()
        {
            return await _repository.GetAllAsync();
        }

        public async Task<SteadModel> GetSteadAsync(int id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public async Task AddSteadAsync(SteadModel stead)
        {
            await _repository.AddAsync(stead);
        }

        public async Task ChangeSteadAsync(int id, SteadModel stead)
        {
            await _repository.UpdateAsync(id, stead);
        }

        public async Task RemoveSteadAsync(int id)
        {
            await _repository.DeleteAsync(id);
        }
    }
}
