﻿using Domain.Entities;

namespace Infrastructure
{
    public class RReestrObjectDao : GardenDaoBase
    {
        public RReestrObjectDao(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public RReestrObject FindById(long rReestrObjectId) => GardenCalendaryContext.RReestrObject.FirstOrDefault(it => it.Id == rReestrObjectId);

        public void Add(RReestrObject rReestrObject)
        {
            GardenCalendaryContext.RReestrObject.Add(rReestrObject);
            SaveChanges();
        }
    }
}
