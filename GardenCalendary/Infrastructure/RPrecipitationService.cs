﻿using Application;
using Domain.Entities;
using Infrastructure.Interfaces;

namespace Infrastructure
{
    public class RPrecipitationService : GardenServiceBase, IRPrecipitationService
    {
        public RPrecipitationService(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public RPrecipitation FindById(long rPrecipitationId)
        {
            return GardenDaoRegistry.RPrecipitationDao.FindById(rPrecipitationId);
        }

        public void Add(RPrecipitation rPrecipitation)
        {
            GardenDaoRegistry.RPrecipitationDao.Add(rPrecipitation);
        }
    }
}
