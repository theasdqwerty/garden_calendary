﻿using Domain;

namespace Infrastructure
{
    public class GardenDaoRegistry
    {
        public GardenCalendaryContext GardenCalendaryContext { get; private set; }

        public GardenDaoRegistry(GardenCalendaryContext gardenCalendaryContext) => GardenCalendaryContext = gardenCalendaryContext;

        private PlantDao _plantDao;
        public PlantDao PlantDao => _plantDao ?? (_plantDao = new PlantDao(GardenCalendaryContext));

        private RegionDao _regionDao;
        public RegionDao RegionDao => _regionDao ?? (_regionDao = new RegionDao(GardenCalendaryContext));

        private RPrecipitationDao _rPrecipitationDao;
        public RPrecipitationDao RPrecipitationDao => _rPrecipitationDao ?? (_rPrecipitationDao = new RPrecipitationDao(GardenCalendaryContext));

        private RReestrObjectDao _rReestrObjectDao;
        public RReestrObjectDao RReestrObjectDao => _rReestrObjectDao ?? (_rReestrObjectDao = new RReestrObjectDao(GardenCalendaryContext));

        private SteadDao _steadDao;
        public SteadDao SteadDao => _steadDao ?? (_steadDao = new SteadDao(GardenCalendaryContext));

        private UserDao _userDao;
        public UserDao UserDao => _userDao ?? (_userDao = new UserDao(GardenCalendaryContext));

        private WeatherCalendarDao _weatherCalendarDao;
        public WeatherCalendarDao WeatherCalendarDao => _weatherCalendarDao ?? (_weatherCalendarDao = new WeatherCalendarDao(GardenCalendaryContext));

    }
}
