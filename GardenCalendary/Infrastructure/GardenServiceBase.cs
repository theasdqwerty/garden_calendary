﻿using Domain;


namespace Infrastructure
{
    public class GardenServiceBase
    {
        public GardenCalendaryContext GardenCalendaryContext { get; }

        public GardenServiceBase(GardenCalendaryContext gardenCalendaryContext) => GardenCalendaryContext = gardenCalendaryContext;


        private GardenDaoRegistry _gardenDaoRegistry;
        public GardenDaoRegistry GardenDaoRegistry => _gardenDaoRegistry ?? (_gardenDaoRegistry = new GardenDaoRegistry(GardenCalendaryContext));
    }
}
