﻿using Domain.Entities;

namespace Infrastructure
{
    public class WeatherCalendarDao : GardenDaoBase
    {
        public WeatherCalendarDao(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public WeatherCalendar FindById(long weatherCalendarId) => GardenCalendaryContext.WeatherCalendar.FirstOrDefault(it => it.Id == weatherCalendarId);

        public void Add(WeatherCalendar user)
        {
            GardenCalendaryContext.WeatherCalendar.Add(user);
            SaveChanges();
        }
    }
}
