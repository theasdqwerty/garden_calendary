﻿using Application;
using Domain.Entities;
using Infrastructure.Interfaces;

namespace Infrastructure
{
    public class RReestrObjectService : GardenServiceBase, IRReestrObjectService
    {
        public RReestrObjectService(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public RReestrObject FindById(long rReestrObjectId)
        {
            return GardenDaoRegistry.RReestrObjectDao.FindById(rReestrObjectId);
        }

        public void Add(RReestrObject rReestrObject)
        {
            GardenDaoRegistry.RReestrObjectDao.Add(rReestrObject);
        }
    }
}
