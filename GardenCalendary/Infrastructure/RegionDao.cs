﻿using Domain.Entities;

namespace Infrastructure
{
    public class RegionDao : GardenDaoBase
    {
        public RegionDao(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public Region FindById(long regionId) => GardenCalendaryContext.Region.FirstOrDefault(it => it.RegionCodeId == regionId);

        public void Add(Region region)
        {
            GardenCalendaryContext.Region.Add(region);
            SaveChanges();
        }
    }
}
