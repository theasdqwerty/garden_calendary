﻿using Application;
using Domain.Entities;
using Infrastructure.Interfaces;

namespace Infrastructure
{
    public class PlantService : GardenServiceBase, IPlantService
    {
        public PlantService(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public Plant FindById(long plantId)
        {
            return GardenDaoRegistry.PlantDao.FindById(plantId);
        }

        public void Add(Plant plant)
        {
            GardenDaoRegistry.PlantDao.Add(plant);
        }
    }
}
