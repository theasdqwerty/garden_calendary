﻿using Application;
using Domain.Entities;
using Infrastructure.Interfaces;

namespace Infrastructure
{
    public class SteadService : GardenServiceBase, ISteadService
    {
        public SteadService(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public Stead FindById(long steadId)
        {
            return GardenDaoRegistry.SteadDao.FindById(steadId);
        }

        public void Add(Stead stead)
        {
            GardenDaoRegistry.SteadDao.Add(stead);
        }
    }
}
