﻿using Application;
using Domain.Entities;
using Infrastructure.Interfaces;

namespace Infrastructure
{
    public class RegionService : GardenServiceBase, IRegionService
    {
        public RegionService(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public Region FindById(long regionId)
        {
            return GardenDaoRegistry.RegionDao.FindById(regionId);
        }

        public void Add(Region region)
        {
            GardenDaoRegistry.RegionDao.Add(region);
        }
    }
}
