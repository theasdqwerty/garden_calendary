﻿using Domain;

namespace Infrastructure
{

    public class GardenDaoBase
    {
        public GardenCalendaryContext GardenCalendaryContext { get; private set; }

        public GardenDaoBase(GardenCalendaryContext gardenCalendaryContext)
        {
            GardenCalendaryContext = gardenCalendaryContext;
        }

        public void SaveChanges()
        {
            GardenCalendaryContext.SaveChanges();
        }
    }
}
