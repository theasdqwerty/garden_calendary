﻿using Domain.Entities;

namespace Infrastructure
{
    public class PlantDao : GardenDaoBase
    {
        public PlantDao(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public Plant FindById(long plantId) => GardenCalendaryContext.Plant.FirstOrDefault(it => it.Id == plantId);

        public void Add(Plant plant)
        {
            GardenCalendaryContext.Plant.Add(plant);
            SaveChanges();
        }
    }
}
