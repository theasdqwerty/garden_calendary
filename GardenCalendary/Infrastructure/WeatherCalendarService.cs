﻿using Application;
using Domain.Entities;
using Infrastructure.Interfaces;

namespace Infrastructure
{
    public class WeatherCalendarService : GardenServiceBase, IWeatherCalendarService
    {
        public WeatherCalendarService(GardenCalendaryContext gardenCalendaryContext) : base(gardenCalendaryContext) { }

        public WeatherCalendar FindById(long weatherCalendarId)
        {
            return GardenDaoRegistry.WeatherCalendarDao.FindById(weatherCalendarId);
        }

        public void Add(WeatherCalendar weatherCalendar)
        {
            GardenDaoRegistry.WeatherCalendarDao.Add(weatherCalendar);
        }
    }
}
