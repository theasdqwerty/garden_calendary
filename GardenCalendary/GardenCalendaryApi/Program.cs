using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using Application.Services;
using Domain;
using Infrastructure;
using Infrastructure.Interfaces;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Domain.Entities;
using Domain.Interfaces;

var configurationBuilder = new ConfigurationBuilder();
var config = configurationBuilder.AddJsonFile("appsettings.json").Build();
var connectionString = config[$"ConnectionStrings:PostgreSQL"];


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<GardenCalendaryContext>(o => o.UseNpgsql(connectionString));
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IAccuWeatherApiService, AccuWeatherApiService>();
builder.Services.AddScoped<Application.Interfaces.Services.ISteadService, Application.Services.SteadService>();

builder.Services.AddScoped<IService<Plant>, Application.Services.PlantService>();

builder.Services.AddTransient<DbContext, GardenCalendaryContext>();
builder.Services.AddTransient<IRepository<Plant>, Repository<Plant>>();
builder.Services.AddTransient<ISteadRepository, SteadRepository>();
builder.Services.AddTransient(typeof(IGenericRepository<,>), typeof(GenericRepository<,>));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();




static void ReturnAllLabelsInConsole(object entity)
{
    IEnumerable<FieldInfo> fields = entity.GetType().GetTypeInfo().DeclaredFields;

    foreach (var field in fields.Where(x => !x.IsStatic))
    {
        Console.WriteLine("{0}  =  {1}", field.Name, (entity != null) ? field.GetValue(entity) : string.Empty);
    }
}