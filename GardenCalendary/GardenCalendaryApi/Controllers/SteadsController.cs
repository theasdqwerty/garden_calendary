﻿using Application.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_project.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SteadsController : ControllerBase
    {
        private readonly ISteadService _steadService;

        public SteadsController(ISteadService steadService)
        {
            _steadService = steadService;
        }

        [HttpGet]
        public async Task<ActionResult<IList<SteadModel>>> GetAll()
        {
            return Ok(await _steadService.GetAllSteadsAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SteadModel>> GetById(int id)
        {
            return Ok(await _steadService.GetSteadAsync(id));
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] SteadModel stead)
        {
            await _steadService.AddSteadAsync(stead);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Change(int id, [FromBody] SteadModel stead)
        {
            await _steadService.ChangeSteadAsync(id, stead);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Remove(int id)
        {
            await _steadService.RemoveSteadAsync(id);
            return Ok();
        }
    }
}
