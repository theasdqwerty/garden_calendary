using API_project.Models;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace API_project.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PlantController : ControllerBase
{
    private readonly IService<Plant> _plantService;

    public PlantController(IService<Plant> plantService) 
        => _plantService = plantService;

    [HttpGet]
    public ActionResult<IEnumerable<PlantModel>> GetAll()
        => Ok(_plantService.GetAll());

    [HttpGet("{id}")]
    public async Task<ActionResult<PlantModel>> GetById(int id) 
        => Ok(await _plantService.GetById(id));

    [HttpPost]
    public async Task<ActionResult> Create([FromBody] PlantModel plantModel)
    {
        await _plantService.Insert(plantModel.AsPlant());
        return Ok();
    }

    [HttpPut("{id}")]
    public async Task<ActionResult> Update(int id, [FromBody] PlantModel plantModel)
    {
        await _plantService.Update(id, plantModel.AsPlant());
        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        await _plantService.Delete(id);
        return Ok();
    }
}