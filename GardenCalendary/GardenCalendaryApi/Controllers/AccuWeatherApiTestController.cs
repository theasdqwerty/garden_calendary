﻿using Application.Interfaces.Services;
using Domain.Models;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GardenCalendaryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccuWeatherApiTestController : ControllerBase
    {
        private IAccuWeatherApiService _apiService { get; set; }

        public AccuWeatherApiTestController(IAccuWeatherApiService apiService)
        {
            _apiService = apiService;
        }

        [HttpPost]
        public IActionResult GetCityFromServices([FromBody] GeoPositionModel geoPosition)
        {
            string langauage = "ru";
            bool details = true;
            bool toplevel = false;
            _apiService.GetCityFromServices(geoPosition, langauage, details, toplevel);
            return Ok();
        }
    }
}
