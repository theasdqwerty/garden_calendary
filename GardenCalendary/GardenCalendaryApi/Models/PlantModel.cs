using AutoMapper;
using Domain.Entities;

namespace API_project.Models;

public class PlantModel
{
    public int Id { get; set; }

    public string? PlantName { get; set; }

    public int? TempMin { get; set; }

    public int? TempMax { get; set; }

    public int? SoilMoistureMin { get; set; }

    public int? SoilMoistureMax { get; set; }

    public Plant AsPlant()
    {
        var mapperConfiguration = new MapperConfiguration(c => c.AddProfile(new Mapping()));
        var mapper = mapperConfiguration.CreateMapper();
        return mapper.Map<Plant>(this);
    }
    
    private class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Plant, PlantModel>();
        }
    }
}
